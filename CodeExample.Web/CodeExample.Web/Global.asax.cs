﻿using CodeExample.Managers.ScrapeManagers;
using CodeExample.Managers.SimpleScrapeManager;
using CodeExample.Services.SimpleHttpFetcher;
using CodeExample.Services.WebFetch;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

namespace CodeExample.Web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
            RegisterIoC();
            RegisterViewLocations();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //do some logging
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            AreaRegistration.RegisterAllAreas();

            //this route should flop
            //we dont have a global space setup...we only have the codeexample area... no need for a default route registration
            //routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Scraper", action = "Index", id = "" });
        }

        private void RegisterIoC()
        {
            var container = new Container();

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly()); //get all the controllers in this assembly (CodeExample.Web)

            container.Register<IWebFetcher, SimpleHttpFetcher>();
            container.Register<IScrapeManager, WebScrapeManager>();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private void RegisterViewLocations()
        {
            //alter razerengine with additional view location formats
            //var razorEngine = ViewEngines.Engines.OfType<RazorViewEngine>().FirstOrDefault();
        }
    }
}