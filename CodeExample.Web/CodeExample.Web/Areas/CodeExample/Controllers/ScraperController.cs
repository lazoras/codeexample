﻿using CodeExample.Managers.ScrapeManagers;
using CodeExample.Managers.ScrapeManagers.Models;
using CodeExample.Web.Areas.CodeExample.Models.Scraper;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CodeExample.Web.Areas.CodeExample.Controllers
{
    [RouteArea("CodeExample")]
    public class ScraperController : Controller
    {
        protected readonly IScrapeManager ScrapeManager;
        public ScraperController(IScrapeManager scrapeManager)
        {
            ScrapeManager = scrapeManager;
        }
        
        public ActionResult Index()
        {
            var vm = new ScraperViewModel();
            InitializeScraperView(vm);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Index(string url)
        {
            var data = ScrapeManager.ScrapeIt(url);

            var vm = new ScraperViewModel();
            InitializeScraperView(vm, data);
            vm.SearchUrl = url;


            return View(vm);
        }

        public ActionResult Test()
        {
            return View();
        }

        private void InitializeScraperView(ScraperViewModel vm, ScrapeData data = null)
        {
            //get this stuff from CMS, database, configuration, static class hard code mappings, etc...
            vm.SubmitBtnTxt = "Submit";
            vm.SearchUrl = "";

            vm.ImgUrls = data?.ImgUrls ?? new List<string>();
            vm.Words = data?.OrderedWords ?? new Dictionary<string, int>();
            vm.TotalWords = data?.TotalWords ?? 0;
        }


    }
}