﻿using System.Collections.Generic;

namespace CodeExample.Web.Areas.CodeExample.Models.Scraper
{
    public class ScraperViewModel
    {
        public List<string> ImgUrls { get; set; } //this is a list because its originally a list, and we need an index later (yes we could use a counter and standardize the IEnumerable on the model)
        public int TotalWords { get; set; }
        public IEnumerable<KeyValuePair<string, int>> Words { get; set; }

        /////////
        // we would do alot more than this normally... labels... limits, texts...etc...etc
        ////////
        public string SubmitBtnTxt { get; set; }
        public string SearchUrl { get; set; }
    }
}