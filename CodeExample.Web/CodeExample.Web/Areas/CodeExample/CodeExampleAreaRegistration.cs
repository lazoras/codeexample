﻿using System.Web.Mvc;

namespace CodeExample.Web.Areas.CodeExample
{
    public class CodeExampleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "CodeExample";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CodeExample_default",
                //"CodeExample/{controller}/{action}/{id}",
                "{controller}/{action}/{id}", //this area is now our default route
                new { controller = "Scraper", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}