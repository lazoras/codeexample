﻿namespace CodeExample.Services.WebFetch
{
    public interface IWebFetcher
    {
        string FetchResponse(string url);
    }
}
