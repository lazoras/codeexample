﻿using System;

namespace CodeExample.Managers.ScrapeManagers.Exceptions
{
    public class SomeSortOfErrorException : Exception
    {
        public SomeSortOfErrorException(string message) : base(message) { }
        public SomeSortOfErrorException(string message, Exception innerException) : base(message, innerException) { }
    }
}
