﻿using CodeExample.Managers.ScrapeManagers.Models;

namespace CodeExample.Managers.ScrapeManagers
{
    public interface IScrapeManager
    {
        ScrapeData ScrapeIt(string uri);
    }
}
