﻿using System.Collections.Generic;

namespace CodeExample.Managers.ScrapeManagers.Models
{
    public class ScrapeData
    {
        public List<string> ImgUrls { get; set; }
        public int TotalWords { get; set; }
        public Dictionary<string, int> Words { get; set; } //this technically could be moved out but lets leave it here for simplicity
        public IEnumerable<KeyValuePair<string,int>> OrderedWords { get; set; }
        
    }
}
