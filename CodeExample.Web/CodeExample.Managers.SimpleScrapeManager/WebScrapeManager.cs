﻿using CodeExample.Managers.ScrapeManagers;
using CodeExample.Managers.ScrapeManagers.Exceptions;
using CodeExample.Managers.ScrapeManagers.Models;
using CodeExample.Services.WebFetch;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

//i would say this would actually be CodeExample.Managers.ScrapeManagers.SimpleScrapeManager
//but i messed it up when i made the library
namespace CodeExample.Managers.SimpleScrapeManager
{
    public class WebScrapeManager : IScrapeManager
    {
        protected readonly IWebFetcher WebFetcher;
        public WebScrapeManager(IWebFetcher webFetcher)
        {
            WebFetcher = webFetcher;
        }

        public ScrapeData ScrapeIt(string uri)
        {
            var result = new ScrapeData();
            result.ImgUrls = new List<string>();
            result.Words = new Dictionary<string, int>();
            result.TotalWords = 0;

            var response = WebFetcher.FetchResponse(uri);

            if (string.IsNullOrWhiteSpace(response))
            {
                //some might argue we lost the stack trace...which is true... but we could just put the try/catch here ..which it might be better here...
                throw new SomeSortOfErrorException("Some error occurred that i cant tell you the details about. however, i didnt build a graceful way to handle it either... so here ya go");
            }

            //we could have just loaded it right here but im trying to demonstrate that i know there should be a data layer....
            //var doc = new HtmlWeb();
            //doc.Load(uri);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);
            
            var nodes = doc.DocumentNode.DescendantsAndSelf();
            foreach(var node in nodes)
            {
                ProcessElement(node, result);
            }

            //its debatable if the sort and limit should happen in the logic layer or in the UI layer
            //but if we were considering. as a company, things of this nature are business logic...it would happen here
            //along with other logics
            result.OrderedWords = result.Words.OrderByDescending(x => x.Value).Take(10);

            return result;
        }

        protected void ProcessElement(HtmlNode el, ScrapeData dataObj)
        {
            switch (el.NodeType)
            {
                case HtmlNodeType.Element:
                    {
                        if(string.Compare(el.Name, "img", true) == 0 && el.Attributes.Contains("src")) //note... might have to do a case insensitive version of attribute contains
                        {
                            var src = el.GetAttributeValue("src", "");
                            if(!string.IsNullOrWhiteSpace(src))
                            {
                                dataObj.ImgUrls.Add(src);
                            }
                        }
                    }
                    break;
                case HtmlNodeType.Text:
                    {
                        var words = Regex.Split(el.InnerText, @"\s").Where(x => Regex.IsMatch(x, @"\S+"));
                        dataObj.TotalWords += words.Count();
                        foreach(var word in words)
                        {
                            if(dataObj.Words.ContainsKey(word))
                            {
                                dataObj.Words[word]++;
                            }else
                            {
                                dataObj.Words[word] = 1;
                            }
                        }
                    }
                    break;
                default:
                    {

                    }
                    break;
            }
        }


    }
}
