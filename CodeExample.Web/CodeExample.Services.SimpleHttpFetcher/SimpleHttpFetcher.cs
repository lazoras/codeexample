﻿using CodeExample.Services.WebFetch;
using System;
using System.Net.Http;

//i would say this would actually be CodeExample.Services.WebFetch.SimpleHttpFetcher
//but i messed it up when i made the library
namespace CodeExample.Services.SimpleHttpFetcher
{
    public class SimpleHttpFetcher : IWebFetcher
    {
        protected readonly HttpClient Client;
        public SimpleHttpFetcher()
        {
            Client = new HttpClient();
        }
        public string FetchResponse(string url)
        {
            //obviously there is alot of error checking and try catch so that we can distill a nice clean error message up the chain
            //i didnt put a logger for the catch...hiding an exception within a catch without logging is is bad
            //malformed urls and invalid URLS are most common
            //reaon...time...
            try
            {


                var fetchTask = Client.GetAsync(url);

                fetchTask.Wait(10000); //ten seconds
                var result = fetchTask.Result;

                var readTask = result.Content.ReadAsStringAsync();
                readTask.Wait(10000); //ten seconds
                return readTask.Result;
            }catch(Exception e)
            {
                //log it 

                //maybe throw a special generic exception... or return null....
                return null;
            }

        }
    }
}
