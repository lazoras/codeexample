# CodeExample README

## Original Task Overview:
Write a small user friendly program that allows the user to type a URL for a given web page and the program will then do the following:

- List all images and show them to the user appropriately in a carousel or gallery control of some kind (borrow from the internet or write your own)
- Count all the words (display the total) and display the top 10 occurring words and their count (either as a table or chart of some kind, again you choose or write your own)

There is a lot of room within that brief for Robert to demonstrate his strengths with some creative coding.
Code quality and non-functional aspects are more important than creating a visually impressive solution.

Candidates that do well on this test generally impress us in multiple non-functional areas (examples only):

Method of delivering or displaying the program to us
Choice of tools & frameworks
Consideration for Security, Performance, Maintainability, Re-usability, etc
He may optionally include a short description highlighting the areas of his program that he thinks are unique/important.

## Codebase Overview
There are two projects. This was actually an accident because i wrote the whole thing in javascript first. I was thinking to myself WHY would they give me a FED task when im supposed to be a mostly back end developer??? In my mind, this task was better suited to be a client side operation (there is no way we would want an end user issueing crawls to other sites right??)

I then realized its just a code example and i should probably rewrite it deomonstrating my ability to make a C# project. To which, i thought of making this a dotnetfiddle, but i was told i should try to demonstrate more complicated scenarios..

I used comments to express where things are over-engineered on purpose for demonstration and for places where if this were a full blown application there would be additional features (like a logger).

### FED
- Uses Javascript to complete the task goals
- Deomonstrates ability to write full blown javascript classes and infrastructure.
- Demonstrates ability to manipulate data, employ templating techniqes, bundling, namespaces, etc, etc

#### Run it using:
```
npm install
npm run start
```
There is an example `test.html` page setup to be scraped.

### Web
- Uses C#/.NET Framework to complete the task goals
- Deomonstrates IoC, 3 layers of MVC, abstraction, SoC, OOP, etc, etc

#### Run it using:
```
Visual Studio + IISExpress
The url should be the root (/)
There is a /scraper/test action setup to be scraped.
```





