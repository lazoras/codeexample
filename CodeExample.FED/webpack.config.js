const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const outputPath = path.resolve(__dirname, "dist");

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: outputPath
  },
  module : {
    rules: [{
      test: /\.scss$/,
      use: ["style-loader", "css-loader", "sass-loader"]
    },
  {
    test: /\.css$/,
    use: ["style-loader", "css-loader"]
  }]
  },
  resolve : {
    alias: {
      components : "./components"
    }
  },
  devServer: {
     contentBase : path.resolve(__dirname,'test'),
     watchContentBase: true,
     headers: {
       //'Access-Control-Allow-Origin' : '*' //to enable scraping on any site!
     }
  },
  devtool: 'source-map',
  mode : "development",
  plugins: [new HtmlWebpackPlugin({
    template: path.resolve(__dirname,'devTemplate.html'),
    filename : 'index.html'
  })]
};