import $ from "jquery";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import scraper from "components/scraper";

$(document).ready(function(){
    initIf('.formScraper', scraper);
});

function initIf(target, component)
{
    var result = [];
    var objs = $(target);
    if(objs.length >= 1)
    {
        for(var count = 0; count < objs.length; count++)
        {
            var obj = objs[count];
            result.push(new component(obj));
        }
    }

    return result;
}