"use strict"
import $ from "jquery"
var jsrender = require("jsrender")($);

import viewmodel from "./scraper.vm"
import "./scraper.scss"

export default class Scraper {
    constructor (element){
        this.$root = $(element);
        this.$form = this.$root.find('form');
        this.$txtBox = this.$form.find('*[name=pageSrcBx]');
        this.$results = this.$root.find('.results');
        this.$resultsTable = this.$results.find('.resultsTable');
        this.$resultsGallery = this.$results.find('.gallery');
        this.$resultsLoader = this.$root.find('.resultsLoader');
        this.fetching = false;
        this.fetchingUrl = '';
        this.topWords = 10;

        this.$form.submit(this.submitHandler.bind(this));
        let templateHtml = this.$root.find('.resultTemplate').html();
        this.resultTemplate = jsrender.templates(templateHtml);
    }

    submitHandler(event){
        event.preventDefault();
        try{            
        var url = this.$txtBox.val();
        //do some validation
        if(this.fetching === true && this.fetchingUrl === url)
        {
            return; //we dont want to spam...but we dont need to let the user know this
        }
        this.fetching = true;
        this.fetchingUrl = url;

        var data = this.fetchPageData(url);
        data = this.manipulatePageData(data);
        
        this.displayData(data);
        }finally
        {
            this.fetching = false;
        }
    }

    //when/if we swap to use our own server we would do it here...
    fetchPageData(url) {
        
        //set a timer for the loader
        //this.loaderHandler = setTimeout blah blah balh

        //hide results table /gallery
        this.$results.hide();

        //fetchData
        let result = null;
        $.get({
            url : url,
            async: false
        }).always(function(response){
            result = response;
        });

        return result;
    }

    showLoader(){

    }

    manipulatePageData(pageData){
        //crawl dom looking for img elements
        let vm = new viewmodel([],0,{});
        let $pageData = $.parseHTML(pageData);
        $.each($pageData, (index, obj) => this.crawlDOM(obj, vm) );
        
        //this sort could be moved into the vm and treated as "rendering code" for the template
        vm.sortedWords = Object.keys(vm.words).sort(function(a, b) {
            return vm.words[b] - vm.words[a];
        });

        return vm;
    }

    crawlDOM(el, vm)
    {
        if(!el || el === undefined)
        {
            return;
        }

        this.processDomElem(el, vm);

        $.each(el.childNodes, (index, child) => this.crawlDOM(child, vm) );
    }

    processDomElem(el, vm)
    {
        //nodeType:1 element (we need to check the name for the image element)
        //nodeType:3 is text
        switch(el.nodeType)
        {
            case 1: //Element Type
                //this is an element we need to look at the tag name
                if(el.tagName === "IMG" && el.attributes["src"])
                {
                    let src = el.attributes["src"].value;
                    //do some checking with the src vs the requested url and format appropriately
                    //combine original url and src if the url is relative
                    vm.imgUrls.push(src);
                }
                break;
            case 3: //Text Type
                
                //this is an element that has text
                let wordFilter = new RegExp('\\S+'); //probably could make this a property on the class or something
                
                //get the whole text, split it into words via white space, filter out any weird things like tabs, returns etc...
                let words = el.textContent.split(/\s+/i).filter( (word) => wordFilter.test(word) );

                //record the total amount of words
                vm.totalWords += words.length;

                //tabulate the words
                $.each(words, (index, obj) => {
                    if(!vm.words[obj])
                    {
                        vm.words[obj] = 1;
                    }else
                    {
                        vm.words[obj]++;
                    }
                });
                break;
            default:
                break;
        }
    }

    displayData(vm){
        
        let html = this.resultTemplate.render(vm);
        this.$results.html(html);

        //cancel loader
        //hide loader

        //show results table/ gallery
        this.$results.show();
    }


}